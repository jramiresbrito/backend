FactoryBot.define do
  factory :comment do
    sequence(:name) { |n| "Name #{n}" }
    content { Faker::Lorem.paragraph }
    email { Faker::Internet.email }
    event
  end
end
