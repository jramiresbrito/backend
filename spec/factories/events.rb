FactoryBot.define do
  factory :event do
    sequence(:name) { |n| "Name #{n}" }
    description { Faker::Lorem.paragraph }
    type { %w[museu teatro concerto].sample }
    url { Faker::Internet.url }
    continuous { [true, false].sample }
    starts_at { Date.tomorrow }
    finishes_at { Date.tomorrow + 3.days }
    user
  end
end
