module ResourceHashes
  # rubocop:disable Metrics/MethodLength
  def build_events_hash(events, single: false)
    events_hash = events.map do |e|
      e.attributes.except('created_at', 'updated_at')
    end
    events_hash.each do |e|
      id = e['_id'].to_s
      e.delete('_id')
      e['id'] = id
      e['attributes'] = { 'name' => e['name'],
                          'description' => e['description'],
                          'type' => e['type'],
                          'url' => e['url'],
                          'continuous' => e['continuous'],
                          'starts-at' => e['starts_at'].strftime('%Y-%m-%d'),
                          'finishes-at' => e['finishes_at'].strftime('%Y-%m-%d') }
      e['relationships'] = { 'comments' => { 'data' => [] } }
      e['type'] = 'events'
      e.delete('name')
      e.delete('description')
      e.delete('url')
      e.delete('continuous')
      e.delete('starts_at')
      e.delete('finishes_at')
    end

    single ? { "data" => events_hash[0] } : { "data" => events_hash }
  end
  # rubocop:enable Metrics/MethodLength

  def build_comments_hash(comments, single: false)
    comments_hash = comments.map do |e|
      e.attributes.except('created_at', 'updated_at')
    end
    comments_hash.each do |c|
      comment_id = c['_id'].to_s
      c.delete('_id')
      c['id'] = comment_id
      c['type'] = 'comments'
      c['attributes'] = { 'name' => c['name'], 'content' => c['content'], 'email' => c['email'] }
      c.delete('name')
      c.delete('content')
      c.delete('email')
      event_id = c['event_id'].to_s
      c.delete('event_id')
      c['relationships'] = { 'event' => { 'data' => { 'id' => event_id, 'type' => 'events' } } }
    end

    single ? { "data" => comments_hash[0] } : { "data" => comments_hash }
  end
end

RSpec.configure do |config|
  config.include ResourceHashes, type: :request
end
