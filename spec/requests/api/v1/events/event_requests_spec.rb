require 'spec_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'API V1 Events', type: :request do
  context 'GET /events' do
    let(:url) { '/api/v1/events' }
    let!(:events) do
      events = []
      15.times { events << create(:event) }
      build_events_hash(events)
    end

    context 'without any params' do
      before(:each) { get url }

      it 'returns events limited by default pagination' do
        expected_events = events['data'][5..].reverse!

        json_body['data'].each_with_index do |event, index|
          expect(event).to eq expected_events[index]
        end
      end

      it 'returns success status' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with search[name] param' do
      let!(:events_by_name) do
        events = []
        15.times { |n| events << create(:event, name: "Search #{n + 1}") }
        build_events_hash(events)
      end

      it 'returns only searched events limited by default pagination' do
        get url, headers: header, params: { search: { name: 'Search' } }

        expected_events = events_by_name['data'][5..].reverse!

        json_body['data'].each_with_index do |event, index|
          expect(event).to eq expected_events[index]
        end
      end

      it 'returns an empty array when none record matches the criteria' do
        get url, headers: header, params: { search: { name: 'Foo' } }

        expect(json_body['data']).to eq []
      end

      it 'returns success status' do
        get url, headers: header, params: { search: { name: 'Search' } }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with search[type] param' do
      let!(:events_by_name) do
        events = []
        15.times { |n| events << create(:event, type: "Type #{n + 1}") }
        build_events_hash(events)
      end

      let(:page_number) { 2 }

      it 'returns only searched events limited by following pagination' do
        get url, headers: header, params: { search: { type: 'Type' }, page: { number: page_number } }

        expected_events = events_by_name['data'][0..4].reverse!

        json_body['data'].each_with_index do |event, index|
          expect(event).to eq expected_events[index]
        end
      end

      it 'returns an empty array when any records matches the criteria' do
        get url, headers: header, params: { search: { type: 'Foo' } }

        expect(json_body['data']).to eq []
      end

      it 'returns success status' do
        get url, headers: header, params: { search: { type: 'Type' } }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  context 'POST /events' do
    let(:url) { '/api/v1/events' }

    context 'valid params' do
      let(:factory_event_params) { { event: attributes_for(:event) }.to_json }
      let(:event_params) do
        hash = { data: { type: "events" } }
        hash[:data][:attributes] = JSON.parse(factory_event_params)["event"]
        hash.to_json
      end

      it 'adds a new event' do
        expect do
          post url, headers: header, params: event_params
        end.to change(Event, :count).by(1)
      end

      it 'returns the last added event' do
        post url, headers: header, params: event_params
        expected_event = build_events_hash([Event.last], single: true)
        expect(json_body['data']).to eq(expected_event['data'])
      end

      it 'returns success status' do
        post url, headers: header, params: event_params
        expect(response).to have_http_status(:ok)
      end
    end

    context 'invalid params' do
      let(:factory_invalid_params) do
        { event: attributes_for(:event, name: nil, description: nil) }.to_json
      end
      let(:event_invalid_params) do
        hash = { data: { type: "events" } }
        hash[:data][:attributes] = JSON.parse(factory_invalid_params)["event"]
        hash.to_json
      end

      it "shouldn't add a new event" do
        expect do
          post url, headers: header, params: event_invalid_params
        end.to_not change(Event, :count)
      end

      it 'returns error messages' do
        post url, headers: header, params: event_invalid_params
        expect(json_body['errors']['fields']).to have_key('name')
        expect(json_body['errors']['fields']).to have_key('description')
      end

      it 'returns unprocessable_entity' do
        post url, headers: header, params: event_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context 'GET /events/:id' do
    let(:event) { create(:event) }
    let(:url) { "/api/v1/events/#{event.id}" }
    before(:each) { get url, headers: header }

    it 'returns the requested event' do
      expected_event = build_events_hash([event], single: true)
      expect(json_body['data']).to eq(expected_event['data'])
    end

    it 'returns success status' do
      expect(response).to have_http_status(:ok)
    end
  end

  context 'PATCH /events/:id' do
    let(:event) { create(:event) }
    let(:url) { "/api/v1/events/#{event.id}" }

    context 'with valid params' do
      let(:new_name) { 'New Event Name' }
      let(:event_params) do
        { data: { type: "events", attributes: { name: new_name } } }.to_json
      end
      before(:each) { patch url, headers: header, params: event_params }

      it 'updates the event' do
        event.reload
        expect(event.name).to eq new_name
      end

      it 'returns the updated event' do
        expected_event = build_events_hash([event.reload], single: true)
        expect(json_body).to eq(expected_event)
      end

      it 'returns success status' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with invalid params' do
      let(:event_invalid_params) do
        { data: { type: "events", attributes: { name: nil, description: nil } } }.to_json
      end

      it 'does not update the event' do
        old_name = event.name
        old_description = event.description
        patch url, headers: header, params: event_invalid_params
        event.reload
        expect(event.name).to eq old_name
        expect(event.description).to eq old_description
      end

      it 'returns error message' do
        patch url, headers: header, params: event_invalid_params
        expect(json_body['errors']['fields']).to have_key('name')
        expect(json_body['errors']['fields']).to have_key('description')
      end

      it 'returns unprocessable_entity status' do
        patch url, headers: header, params: event_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context 'DELETE /events/:id' do
    let!(:event) { create(:event) }
    let(:url) { "/api/v1/events/#{event.id}" }

    it 'removes the event' do
      expect do
        delete url, headers: header
      end.to change(Event, :count).by(-1)
    end

    it 'returns success status' do
      delete url, headers: header
      expect(response).to have_http_status(:no_content)
    end

    it 'does not return any body content' do
      delete url, headers: header
      expect(json_body).to_not be_present
    end
  end
end
# rubocop:enable Metrics/BlockLength
