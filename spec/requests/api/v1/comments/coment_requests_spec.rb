require 'spec_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'API V1 Comments', type: :request do
  context 'GET /comments' do
    let(:url) { '/api/v1/comments' }
    let!(:comments) do
      comments = []
      15.times { comments << create(:comment) }
      build_comments_hash(comments)
    end
    before(:each) { get url }

    it 'returns comments limited by default pagination' do
      expected_comments = comments['data'][5..].reverse!

      json_body['data'].each_with_index do |comment, index|
        expect(comment).to eq expected_comments[index]
      end
    end

    it 'returns success status' do
      expect(response).to have_http_status(:ok)
    end
  end

  context 'POST /comments' do
    let(:event) { create(:event) }
    let(:url) { "/api/v1/events/#{event.id}/comments" }

    context 'with valid params' do
      let(:factory_invalid_params) { { comment: attributes_for(:comment) }.to_json }
      let(:comment_params) do
        hash = { data: { type: "comments" } }
        hash[:data][:attributes] = JSON.parse(factory_invalid_params)["comment"]
        hash.to_json
      end

      it 'adds a new comment' do
        expect do
          post url, headers: header, params: comment_params
        end.to change(Comment, :count).by(1)
      end

      it 'adds the comment to the event' do
        post url, headers: header, params: comment_params
        expected_comment = build_comments_hash([event.reload.comments.last], single: true)
        expect(json_body).to eq(expected_comment)
      end

      it 'returns success status' do
        post url, headers: header, params: comment_params
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with invalid params' do
      let(:factory_comment_params) { { comment: attributes_for(:comment, name: nil) }.to_json }
      let(:comment_invalid_params) do
        hash = { data: { type: "comments" } }
        hash[:data][:attributes] = JSON.parse(factory_comment_params)["comment"]
        hash.to_json
      end

      it "shouldn't add a new comment" do
        expect do
          post url, headers: header, params: comment_invalid_params
        end.to_not change(Comment, :count)
      end

      it 'returns error messages ' do
        post url, headers: header, params: comment_invalid_params
        expect(json_body['errors']['fields']).to have_key('name')
      end

      it 'returns unprocessable_entity' do
        post url, headers: header, params: comment_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context 'GET /comments/:id' do
    let(:comment) { create(:comment) }
    let(:url) { "/api/v1/comments/#{comment.id}" }
    before(:each) { get url, headers: header }

    it 'returns the requested comment' do
      expected_comment = build_comments_hash([comment], single: true)
      expect(json_body['data']).to eq(expected_comment['data'])
    end

    it 'returns success status' do
      expect(response).to have_http_status(:ok)
    end
  end

  context 'PATCH /comments/:id' do
    let(:comment) { create(:comment) }
    let(:url) { "/api/v1/comments/#{comment.id}" }

    context 'with valid params' do
      let(:new_content) { 'New Comment Content' }
      let(:comment_params) do
        { data: { type: "comments", attributes: { content: new_content } } }.to_json
      end
      before(:each) { patch url, headers: header, params: comment_params }

      it 'updates the comment' do
        comment.reload
        expect(comment.content).to eq new_content
      end

      it 'returns the updated comment' do
        expected_comment = build_comments_hash([comment.reload], single: true)
        expect(json_body).to eq(expected_comment)
      end

      it 'returns success status' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with invalid params' do
      let(:comment_invalid_params) do
        { data: { type: "comments", attributes: { content: nil } } }.to_json
      end

      it 'does not update the comment' do
        old_content = comment.content
        patch url, headers: header, params: comment_invalid_params
        comment.reload
        expect(comment.content).to eq old_content
      end

      it 'returns error message' do
        patch url, headers: header, params: comment_invalid_params
        expect(json_body['errors']['fields']).to have_key('content')
      end

      it 'returns unprocessable_entity status' do
        patch url, headers: header, params: comment_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context 'DELETE /comments/:id' do
    let!(:comment) { create(:comment) }
    let(:url) { "/api/v1/comments/#{comment.id}" }

    it 'removes the comment' do
      expect do
        delete url, headers: header
      end.to change(Comment, :count).by(-1)
    end

    it 'returns success status' do
      delete url, headers: header
      expect(response).to have_http_status(:no_content)
    end

    it 'does not return any body content' do
      delete url, headers: header
      expect(json_body).to_not be_present
    end
  end
end
# rubocop:enable Metrics/BlockLength
