require 'spec_helper'

RSpec.describe Event, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:type) }
  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to validate_presence_of(:continuous) }
  it { is_expected.to validate_presence_of(:starts_at) }

  it { is_expected.to have_many(:comments).with_dependent(:destroy) }

  context 'starts_at:' do
    it "can't be a past date" do
      subject.starts_at = 1.day.ago
      subject.valid?
      expect(subject.errors.keys).to include :starts_at
    end

    it "can't be the current date" do
      subject.starts_at = Time.zone.now
      subject.valid?
      expect(subject.errors.keys).to include :starts_at
    end

    it 'should be valid when the date is in the future' do
      subject.starts_at = Date.tomorrow
      subject.valid?
      expect(subject.errors.keys).to_not include :starts_at
    end
  end

  context 'finishes_at:' do
    it "can't be before or equals starts_at" do
      subject.starts_at = Time.zone.now + 1.hour
      subject.finishes_at = subject.starts_at
      subject.valid?
      expect(subject.errors.keys).to include :finishes_at
    end

    it "shouldn't be required" do
      subject.starts_at = Time.zone.now + 1.hour
      subject.finishes_at = nil
      subject.valid?
      expect(subject.errors.keys).to_not include :finishes_at
    end

    it 'should be valid when the date is after starts_at' do
      subject.starts_at = Date.tomorrow
      subject.finishes_at = Date.tomorrow + 2.days
      subject.valid?
      expect(subject.errors.keys).to_not include :finishes_at
    end
  end
end
