# Peerdustry Fullstack Software Engineer Test

## This API built using Rails allows to create Events and associate comments to it.

The project setup ***will not*** enter on details in order to install the dependencies. Instead i'll provide a recomended (without further explanation) way to install each one.

## Dependencies
* Ruby version: 2.6.6p146

* Rails version: 6.0.3.4

* Bundler version: 2.1.4

* Mongo Version: 4.4.3

* Mongo Image Hash (the most recent on 05-07-2021): "sha256:25dc14159c3dfec8c020ed048dbd883617e5a53367c73bd16ddaa3e303d804d9"

## Project Setup - Development & Tests
The recomended way to setup the database is to use a docker container. The project is currently configured to use the default port for mongodb: 27017. If you want to use another port, please change it in <code>config/mongoid.yml</code>

Having your database up & running, you can install all the dependencies via bundler: <code>bundle install</code>

The next step is to run the test suit: <code>bundle exec rspec</code>. After all tests pass, you can check the test coverage by openning the file: <code>coverage/index.html</code>

You can populate the development and test databases running a special task on: <code>rake dev:prime</code>. This task will generate 20 events with an associated comment. If you want to increase this value, you can change it in <code>lib/tasks/dev/prime.rake</code>

You can start your development server with: <code>rails s</code> which will use the port 3000 by default. If you need to change it, please specify the port on: <code>rails s -p DESIRED_PORT_NUMBER</code>

You are ready.

## Project Setup - Production
On production a small aditional step is necessary. As you can see in: <code>config/mongoid.yml</code>, this project expects a environment variable called <code>ATLAS_URI</code> to be present for production enviroments. You can use a <code>.env</code> file to declare and test it before deploy. A <code>.env.example</code> file its present containing further information. You can rename it to <code>.env</code> or create a new <code>.env</code> and copy the contents from <code>.env.example</code>. I recommend the second alternative.

You are ready to deploy.
