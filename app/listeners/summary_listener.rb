class SummaryListener
  def event_created(event_id)
    event = Event.find(event_id)
    summary = Summary.first # singleton summary created on app setup

    summary.atomically do
      summary.set(event_id: event.id)
      summary.set(number_of_events: Event.count)
    end
  end

  def comment_created(_comment_id)
    Summary.first.set(number_of_comments: Comment.count)
  end

  def user_created(options = {})
    Summary.first.set(number_of_users: User.count)
  end
end
