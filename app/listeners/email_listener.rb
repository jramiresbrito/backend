class EmailListener
  def user_created(options = {})
    send_welcome_email(options[:user_id])
  end

  def reset_password(options = {})
    send_reset_password_email(options[:user_id])
  end

  def comment_created(comment_id)
    send_new_comment_email(comment_id)
  end

  private

  def send_welcome_email(user_id)
    UserMailer.with(user_id: user_id).welcome_email.deliver_later
  end

  def send_reset_password_email(user_id)
    UserMailer.with(user_id: user_id).reset_password.deliver_later
  end

  def send_new_comment_email(comment_id)
    UserMailer.with(comment_id: comment_id).new_comment.deliver_later
  end
end
