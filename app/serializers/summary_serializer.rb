class SummarySerializer < ActiveModel::Serializer
  attributes :id, :number_of_events, :number_of_comments, :number_of_users
  belongs_to :event
end
