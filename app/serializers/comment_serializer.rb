class CommentSerializer < ActiveModel::Serializer
  attributes :id, :name, :content, :email

  belongs_to :event
end
