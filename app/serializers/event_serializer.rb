class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :type, :url, :continuous,
             :starts_at, :finishes_at

  has_many :comments
  belongs_to :user
end
