class UserMailer < ApplicationMailer
  default from: 'eventos@example.com'

  def welcome_email
    @user = User.find(params[:user_id])
    @url  = "#{ENV['FE_WEB_HANDLER_URL']}?action=create&resource_type=users"

    mail(to: @user.email, subject: 'Bem vindo a nossa aplicação de eventos!')
  end

  def reset_password
    @user = User.find(params[:user_id])
    @url  = "#{ENV['FE_WEB_HANDLER_URL']}?action=creation&resource_type=password_recover&token=#{@user.reset_password_token}"

    mail(to: @user.email, subject: 'Mudança de senha')
  end

  def new_comment
    @comment = Comment.find(params[:comment_id])
    @event = @comment.event
    @user = @event.user
    @url  = "#{ENV['FE_WEB_HANDLER_URL']}?action=create&resource_type=comments&resource_id=#{@event.id}"

    mail(to: @user.email, subject: 'Seu evento recebeu um novo comentário :)')
  end
end
