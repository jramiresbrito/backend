class Summary
  include Mongoid::Document
  include Mongoid::Timestamps

  field :number_of_events, type: Integer, default: 0
  field :number_of_comments, type: Integer, default: 0
  field :number_of_users, type: Integer, default: 0
  belongs_to :event, optional: true
end
