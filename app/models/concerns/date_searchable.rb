module DateSearchable
  extend ActiveSupport::Concern
  included do
    scope :filter_date, ->(value) { where({ "starts_at": value.to_s }) }
  end
end
