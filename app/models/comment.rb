class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :event

  field :name, type: String
  field :email, type: String
  field :content, type: String

  validates :name, :content, presence: true
  validates :email, presence: true,
                    length: { maximum: 255 },
                    format: { with: URI::MailTo::EMAIL_REGEXP }
end
