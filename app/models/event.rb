class Event
  include Mongoid::Document
  include Mongoid::Timestamps
  include LikeSearchable
  include DateSearchable

  has_many :comments, dependent: :destroy
  belongs_to :user

  field :name, type: String
  field :description, type: String
  field :type, type: String
  field :url, type: String
  field :continuous, type: Mongoid::Boolean
  field :starts_at, type: Date
  field :finishes_at, type: Date

  validates :description, :type, :url, :continuous, :starts_at, presence: true
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :starts_at, future_date: true
  validate :finishes_at_is_after_starts_at

  private

  def finishes_at_is_after_starts_at
    return if finishes_at.blank?

    errors.add(:finishes_at, 'Must be after starts_at') if finishes_at <= starts_at
  end
end
