module Api::V1
  class ApiController < ApplicationController
    include Wisper::Publisher

    protected

    def create_instance(params, options = {})
      resource_obj = options[:model].new(params)
      resource_obj.send("#{options[:relation]}=", options[:parent]) if options[:parent].present?
      create_resource(resource_obj)
    end

    def create_dependent_instance(params, model, parent_obj, dependent_attr)
      resource_obj = model.new(params)
      create_dependent_resource(resource_obj, parent_obj, dependent_attr)
    end

    def update_instance(resource_obj, resource_params)
      update_resource(resource_obj, resource_params)
    end

    def destroy_instance(resource_obj)
      destroy_resource(resource_obj)
    end

    def set_relation(model, relation_name)
      relation = nil
      relation = params[:include].to_sym if params.key?(:include) && params[:include] == relation_name
      relation.nil? ? model.all : model.all.includes(relation)
    end

    private

    def publish_occurrence(resource_obj)
      publish(:event_created, resource_obj.id.to_s) if resource_obj.is_a?(Event)
      publish(:comment_created, resource_obj.id.to_s) if resource_obj.is_a?(Comment)
    end

    def create_resource(resource_obj)
      if resource_obj.save
        publish_occurrence(resource_obj)
        resource_success(resource_obj, :created)
      else
        resource_errors(resource_obj, :unprocessable_entity)
      end
    end

    def create_dependent_resource(resource_obj, parent_obj, dependent_attr)
      resource_obj.send("#{dependent_attr}=", parent_obj)
      if resource_obj.save
        publish_occurrence(resource_obj)
        resource_success(resource_obj, :created)
      else
        resource_errors(resource_obj, :unprocessable_entity)
      end
    end

    def update_resource(resource_obj, resource_params)
      if resource_obj.update(resource_params)
        resource_success(resource_obj, :ok)
      else
        resource_errors(resource_obj, :unprocessable_entity)
      end
    end

    def destroy_resource(resource_obj)
      if resource_obj.destroy
        head :no_content
      else
        resource_errors(resource_obj, :unprocessable_entity)
      end
    end

    def resource_success(resource_obj, status)
      location_value = "#{request.original_url}/#{resource_obj.id}"
      response.set_header("Location", location_value)

      render json: resource_obj, status: status
    end

    def resource_errors(resource_obj, status)
      render json: resource_obj, status: status,
             serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end
end
