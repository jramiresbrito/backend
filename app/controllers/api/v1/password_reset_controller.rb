module Api::V1
  class PasswordResetController < ApiController
    skip_before_action :authorized

    def create
      email = password_reset_params[:email]
      user = User.find_by(email: email)

      if user
        user.generate_password_token!
        publish(:reset_password, user_id: user.id.to_str)
      end

      head :no_content
    end

    private

    def password_reset_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(normalize_params,
                                                             only: %i[email])
    end

    def normalize_params
      params.to_unsafe_h.deep_transform_keys!(&:underscore)
    end
  end
end
