module Api::V1
  class SummariesController < ApiController
    before_action :set_summary, only: %i[show]

    def index
      relation = policy_scope(set_relation(Summary, 'events'))
      @summary = Api::LoadModelService.new(relation, searchable_params)
      @summary.call
      render json: @summary.records, include: params[:include]
    end

    def show
      authorize @summary
      render json: @summary, include: params[:include]
    end

    private

    def set_summary
      @summary = Summary.find(params[:id])
    end

    def searchable_params
      params.permit({ search: {} }, { page: {} }, :include)
    end
  end
end
