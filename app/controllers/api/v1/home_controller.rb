module Api::V1
  class HomeController < ApiController
    def index
      render json: { message: "Welcome to Peerdustry API" }
    end
  end
end
