module Api::V1
  class PasswordUpdateController < ApiController
    skip_before_action :authorized
    before_action :set_user, only: %i[create]

    def create
      @user.update!(password_update_params)
      @user.clear_password_token!
      head :no_content
    rescue Mongoid::Errors::Validations
      render json: @user, status: :bad_request,
             serializer: ActiveModel::Serializer::ErrorSerializer
    end

    private

    def password_update_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(normalize_params,
                                                             only: %i[password
                                                                      password_confirmation])
    end

    def set_user
      @user = User.find_by(reset_password_token: params.dig(:data, :attributes, :reset_password_token))
      raise ResetPasswordError unless @user.reset_password_token_expires_at > Time.now
    rescue Mongoid::Errors::DocumentNotFound
      raise ResetPasswordError
    end

    def normalize_params
      params.to_unsafe_h.deep_transform_keys!(&:underscore)
    end
  end
end
