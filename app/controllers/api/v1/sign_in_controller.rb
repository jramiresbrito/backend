module Api::V1
  class SignInController < ApiController
    skip_before_action :authorized, only: :create

    def create
      email = sign_in_params[:email]
      password = sign_in_params[:password]

      @user = User.find_by(email: email)
      if @user&.authenticate(password)

        token = encode_token(@user)
        render json: { token: token }, status: :ok
      else
        render_user_error(:credentials, 'are invalid', :unauthorized)
      end
    rescue Mongoid::Errors::DocumentNotFound
      raise InvalidCredentialsError
    end

    private

    def sign_in_params
      params.permit(:email, :password)
    end

    def set_user_error(field, message)
      @user.errors.add(field, message)
    end

    def render_user_error(field, message, status)
      set_user_error(field, message)
      render json: @user, status: status,
             serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end
end
