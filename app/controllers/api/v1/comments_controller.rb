module Api::V1
  class CommentsController < ApiController
    skip_before_action :authorized
    before_action :set_comment, only: %i[show update destroy]
    before_action :set_event, only: %i[create]

    def index
      relation = set_relation(Comment, 'event')
      @comments = Api::LoadModelService.new(relation, searchable_params)
      @comments.call
      render json: @comments.records, include: params[:include]
    end

    def create
      create_dependent_instance(comment_params, Comment, @event, 'event')
    end

    def show
      render json: @comment, include: params[:include]
    end

    def update
      update_instance(@comment, comment_params)
    end

    def destroy
      destroy_instance(@comment)
    end

    private

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_event
      @event = Event.find(params.dig(:data, :relationships, :event, :data, :id))
    end

    def comment_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(normalize_params,
                                                             only: %i[name email content])
    end

    def searchable_params
      params.permit({ search: {} }, { page: {} }, :include)
    end

    def normalize_params
      params.to_unsafe_h.deep_transform_keys!(&:underscore)
    end
  end
end
