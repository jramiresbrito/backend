module Api::V1
  class EventsController < ApiController
    before_action :set_event, only: %i[show update destroy]
    skip_before_action :authorized, only: %i[index show]

    def index
      relation = set_relation(Event, 'comments')
      @events = Api::LoadModelService.new(relation, searchable_params)
      @events.call
      render json: @events.records, include: params[:include]
    end

    def create
      create_instance(event_params, model: Event, parent: @user, relation: "user")
    end

    def show
      render json: @event, include: params[:include]
    end

    def update
      update_instance(@event, event_params)
    end

    def destroy
      destroy_instance(@event)
    end

    def comments
      render json: Event.find(params[:event_id]).comments
    end

    private

    def set_user
      @user = User.find(params.dig(:data, :relationships, :id))
    end

    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(normalize_params,
                                                             only: %i[name description type url
                                                                      continuous starts_at finishes_at])
    end

    def searchable_params
      params.permit({ search: {} }, { page: {} }, :include)
    end

    def normalize_params
      params.to_unsafe_h.deep_transform_keys!(&:underscore)
    end
  end
end
