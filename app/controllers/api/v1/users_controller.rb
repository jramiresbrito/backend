module Api::V1
  class UsersController < ApiController
    skip_before_action :authorized, only: %i[create show]

    def create
      user = User.create(user_params)

      if user.valid?
        publish(:user_created, user_id: user.id.to_str)
        token = encode_token(user)
        render json: { token: token }, status: :created
      else
        render json: user, status: :bad_request,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    def show
      render json: User.find(params[:id]), include: params[:include]
    end

    def me
      render json: set_user
    end

    private

    def user_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(normalize_params,
                                                             only: %i[name email password])
    end

    def normalize_params
      params.to_unsafe_h.deep_transform_keys!(&:underscore)
    end
  end
end
