class TestJob < ApplicationJob
  queue_as :default

  def perform
    puts "Starting the job"
    sleep 2
    puts "Job done"
  end
end
