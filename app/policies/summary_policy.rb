class SummaryPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      event_ids = Event.where(user: user).pluck(:id)
      scope.where(event_id: { "$in" => event_ids })
    end
  end

  def show?
    owner?
  end

  private

  def owner?
    record.event.user == user
  end
end
