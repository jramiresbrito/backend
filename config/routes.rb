require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq"

  namespace :api do
    namespace :v1 do
      get 'home', to: 'home#index'

      resources :users, only: %i[create show] do
        collection do
          get 'me'
        end
      end
      resources :sign_in, only: :create

      resources :events do
        get '/comments', to: 'events#comments'
      end
      resources :comments

      post '/password_reset', to: 'password_reset#create'
      post '/password_update', to: 'password_update#create'

      resources :summaries, only: %i[index show]
    end
  end
end
