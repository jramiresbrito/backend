if Rails.env.development? || Rails.env.test?
  require 'factory_bot'

  namespace :setup do
    desc 'Created/Load application required data'
    task prime: 'db:setup' do
      puts 'Creating singleton summary'
      Summary.create!
      puts 'Done!'
    end
  end

  namespace :dev do
    desc 'Sample data for local development environment'
    task prime: 'db:setup' do
      include FactoryBot::Syntax::Methods

      puts 'Cleaning data...'
      Summary.destroy_all
      User.destroy_all

      puts 'Creating new data'
      summary = Summary.create!

      user1 = create(:user)
      user2 = create(:user)

      20.times do |i|
        event = create(:event, name: "Evento ##{i + 1}", user: [user1, user2].sample)
        10.times do |j|
          create(:comment, name: "Comentário ##{j + 1}", event: event)
        end
      end

      summary.number_of_users = User.count
      summary.number_of_comments = Comment.count
      summary.number_of_events = Event.count
      summary.event = Event.last

      summary.save!

      puts 'done...'
    end
  end
end
