source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

# Basic
gem 'bootsnap', '>= 1.4.2', require: false
gem 'puma', '~> 4.1'
gem 'rails', '~> 6.0.3', '>= 6.0.3.4'

# Authentication
gem 'bcrypt', '~> 3.1.7'
gem 'jwt'

# Authorization
gem 'pundit'

# CORS
gem 'rack-cors'

# Database
gem 'mongoid'

# Pagination
gem 'kaminari-mongoid'

# Events Handling
gem 'wisper-sidekiq'
gem 'sidekiq', '~> 6.1.3'

# Sample Data
gem 'faker'

# Serialization/Rendering
gem 'active_model_serializers', '~> 0.10.0'
gem 'jbuilder'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails'
  gem 'rspec-rails', '~> 4.0.1'
  gem 'factory_bot_rails'
  gem "database_cleaner", '~> 1.99'
  gem "mongoid-rspec"
  gem 'shoulda-matchers', '~> 4.0'
  gem 'simplecov', require: false
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
